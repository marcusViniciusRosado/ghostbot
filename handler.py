#!/usr/bin/env python3

from flask import Flask, request, json
from httplib2 import Http
from json import dumps
from json import loads
from datetime import datetime
import requests

app = Flask(__name__)

@app.route('/bitbucket', methods=['POST'])
def bitbucket():
    if request.method == 'POST':
        http_obj = Http()
        message_headers = {'Content-Type': 'application/json; charset=UTF-8'}
        obj = request.json
        author = obj['pullrequest']['author']['username'] + "\n"
        prUrl = obj['pullrequest']['links']['diff']['href'] + "\n"
        message = author + prUrl
        bot_message = {'text': message}
        urlChat = request.args.get('chatWebhook')
        urlChat += "&token="+request.args.get('token')
        urlChat += "%3D"
        response = http_obj.request(
            uri=urlChat,
            method='POST',
            headers=message_headers,
            body=dumps(bot_message),
        )
        return '', 200
    else:
        abort(400)

@app.route('/webhook', methods=['POST'])
def webhook():
    if request.method == 'POST':
        http_obj = Http()
        message_headers = { 'Content-Type': 'application/json; charset=UTF-8'}
        obj = request.json
        img =  ""
        status = "<b>Test "
        if obj['data']['passing']:
            status += "<font color=\"#00ff00\">Passed:</b> "
            img = "https://i.imgur.com/xdAx3WK.png"
        else:
            status += "<font color=\"#ff0000\">Failed:</b> "
            img = "https://i.imgur.com/wcPhZiD.png"
        testName = obj['data']['test']['name'] + "\n"
        imageUrl = obj['data']['screenshot']['original']['defaultUrl']
        startUrl = "<b>Start Url:</b> " + "<a href=" + obj['data']['startUrl'] + ">" + obj['data']['startUrl'] + "</a>" + "\n"
        i = 0
        erro = ""
        for test in obj['data']['steps']:
            i += 1
            if not test['passing']:
                erro = "Step #" + str(i) + " failed: " + test['command'] + "\n"
                break
        executionTime = "Execution Time: " + str(round(obj['data']['executionTime']/1000, 2)) + "s\n"
        dateExecutionStarted = "Execution Started: " + str(obj['data']['dateExecutionStarted']) + "\n"
        screenshot = "<b>Screenshot:</b>\n"
        message = status + testName + startUrl + erro + executionTime + dateExecutionStarted + screenshot
        bot_message = {
            "cards": [{
                "sections": [
                    {
                        "widgets": [
                            {
                                "image": {
                                    "imageUrl": img
                                }
                            },
                            {
                                "textParagraph": {
                                    "text": message
                                }
                            },
                            {
                                "image": {
                                    "imageUrl": imageUrl,
                                    "onClick": {
                                        "openLink": {
                                            "url": imageUrl
                                        }
                                    }
                                }
                            }

                        ]
                    }
                ]
            }]
        }
        urlChat = request.args.get('chatWebhook')
        urlChat += "&token="+request.args.get('token')
        urlChat += "%3D"
        response = http_obj.request(
            uri=urlChat,
            method='POST',
            headers=message_headers,
            body=dumps(bot_message),
        )
        return str(bot_message), 200
    else:
        abort(400)

@app.route('/', methods=['POST'])
def on_event():
    """Handles an event from Hangouts Chat."""
    event = request.get_json()
    if event['type'] == 'ADDED_TO_SPACE' and event['space']['type'] == 'ROOM':
        text = 'Thanks for adding me to "%s"!' % event['space']['displayName']
    else:
        return
    return json.jsonify({
'text': text
})

if __name__ == '__main__':
    app.run()
